TARGET      = xecrypt.a
SRCDIR      = src
AR          = ar
CC          = gcc
CFLAGS      = -O3 -fopt-info-inline-optimized -msse4.1 -mpclmul -mavx -maes
CXX         = g++
CXXFLAGS    = -Os -std=c++17
LDFLAGS     = 
CXXSRCS     = $(foreach dir,$(SRCDIR), $(wildcard $(dir)/*.cpp))
CXXOBJS     = $(CXXSRCS:.cpp=.o)
CSRCS       = $(foreach dir,$(SRCDIR), $(wildcard $(dir)/*.c))
COBJS       = $(CSRCS:.c=.o)

platform_id != uname -s

release: $(TARGET)

$(TARGET): $(COBJS) $(CXXOBJS)
	$(AR) -rcs $@ $^ #$(CXX)  $^ $(CXXFLAGS) $(LDFLAGS) -o $@

clean:
	rm $(TARGET) $(COBJS) $(CXXOBJS)
